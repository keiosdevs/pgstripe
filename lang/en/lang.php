<?php

return [
    'labels'    => [
        'pluginDesc' => 'Stripe Integration Extension for Keios PaymentGateway',
    ],
    'operators' => [
        'stripe' => 'Stripe',
    ],
    'settings'  => [
        'tab'              => 'Stripe',
        'general'          => 'General settings',
        'apiKey'           => 'Live Publishable ApiKey',
        'secretApiKey'     => 'Live Secret ApiKey',
        'testMode'         => 'Use test mode',
        'testApiKey'       => 'Test Publishable ApiKey',
        'secrettestApiKey' => 'Test Secret ApiKey',
        '3dsecure'         => 'Require 3DSecure Card',
    ],
    'info'      => [
        'header' => 'How to retrieve your Stripe api key',
    ],
];