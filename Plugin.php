<?php namespace Keios\PGStripe;

use Keios\PaymentGateway\Contracts\CreditCardProvider;
use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use Keios\PGStripe\Classes\StripeCreditCardProvider;
use Keios\PGStripe\Components\StripeKeys;
use System\Classes\PluginBase;
use Event;

/**
 * PG-PGStripe Plugin Information File
 *
 * @package Keios\PGStripe
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PG-Stripe',
            'description' => 'keios.pgstripe::lang.labels.pluginDesc',
            'author'      => 'Keios',
            'icon'        => 'icon-stripe'
        ];
    }

    public function registerComponents()
    {
        return [
            StripeKeys::class => 'pgstripe_keys'
        ];
    }

    public function boot()
    {
        \App::bind(CreditCardProvider::class, StripeCreditCardProvider::class);
    }

    public function register()
    {
        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGStripe\Operators\Stripe');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'stripe.general'  => [
                            'label' => 'keios.pgstripe::lang.settings.general',
                            'tab'   => 'keios.pgstripe::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'stripe.info'     => [
                            'type' => 'partial',
                            'path' => '$/keios/pgstripe/partials/_stripe_info.htm',
                            'tab'  => 'keios.pgstripe::lang.settings.tab',
                        ],
                        'stripe.apiKey'   => [
                            'label'   => 'keios.pgstripe::lang.settings.publishableApiKey',
                            'tab'     => 'keios.pgstripe::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'stripe.sapiKey'   => [
                            'label'   => 'keios.pgstripe::lang.settings.secretApiKey',
                            'tab'     => 'keios.pgstripe::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'stripe.testApiKey'   => [
                            'label'   => 'keios.pgstripe::lang.settings.testApiKey',
                            'tab'     => 'keios.pgstripe::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'stripe.stestApiKey'   => [
                            'label'   => 'keios.pgstripe::lang.settings.secrettestApiKey',
                            'tab'     => 'keios.pgstripe::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'stripe.3dsecure' => [
                            'label' => 'keios.pgstripe::lang.settings.3dsecure',
                            'tab'   => 'keios.pgstripe::lang.settings.tab',
                            'type'  => 'switch'
                        ],
                        'stripe.testMode' => [
                            'label' => 'keios.pgstripe::lang.settings.testMode',
                            'tab'   => 'keios.pgstripe::lang.settings.tab',
                            'type'  => 'switch'
                        ],
                    ]
                );
            }
        );
    }
}
