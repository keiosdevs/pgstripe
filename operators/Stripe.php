<?php namespace Keios\PGStripe\Operators;

use Finite\StatefulInterface;
use Keios\PaymentGateway\Support\HashIdsHelper;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Stripe\Source;
use Stripe\ThreeDSecure;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PGStripe\Classes\StripeChargeMaker;
use Keios\PaymentGateway\Core\Operator;
use Stripe\Stripe as StripeApi;
use Stripe\Charge;

/**
 * Class Stripe
 *
 * @package Keios\PGStripe
 */
class Stripe extends Operator implements StatefulInterface
{
    use SettingsDependent;

    const CREDIT_CARD_REQUIRED = true;

    const SECURE_CARD = true;
    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgstripe::lang.operators.stripe';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgstripe/assets/img/stripe/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @return PaymentResponse
     * @throws \Exception
     */
    public function sendPurchaseRequest()
    {
        $this->setApiKey();
        $getData = get();
        $chargeMaker = new StripeChargeMaker($this->cart, $this->paymentDetails, $this->creditCard, $this->uuid);
        $redirectUrl = \Request::url().'?secure_redirect=1&payment_method='.self::$operatorCode;
        if ($order = $this->model->order) {
            if ($order->guest_code) {
                $redirectUrl .= '&guest_code_key='.$order->guest_code;
            }
        }
        if (array_key_exists('secure_redirect', $getData)) {
            $source = Source::retrieve($this->creditCard->getSource());
        } else {
            $sourceArray = [
                'amount'         => $this->cart->getTotalGrossCost()->getAmountBasic() * 100,
                'currency'       => $this->cart->getTotalGrossCost()->getCurrency()->getIsoCode(),
                'type'           => 'three_d_secure',
                'three_d_secure' => [
                    'card' => $this->creditCard->getSource(),
                ],
                'redirect'       => [
                    'return_url' => $redirectUrl,
                ],
            ];
            $source = Source::create(
                $sourceArray
            );
        }

        if ($source->status === 'pending') {
            $redirectUrl = $source->redirect->url;

            $response = new PaymentResponse($this, $redirectUrl);
            $response->setAdditionalStep(true);

            return $response;
        }

        $internalCharge = $chargeMaker->make();
        $internalCharge['source'] = $this->creditCard->getSource();
        //3d secure active?
        $this->getSettings();
        $secureMode = $this->getSettings()->get('stripe.3dsecure');

        try {
            $charge = Charge::create($internalCharge);

        } catch (\Stripe\Error\Card $ex) {
            $message = $this->makeCardErrorMessage($ex);

            return new PaymentResponse($this, null, [$message]);
        } catch (\Exception $e) {
            return new PaymentResponse($this, null, [$e->getMessage()]);
        }

        $this->isPaidInStripe = $charge->paid;
        $this->chargeId = $charge->id;

        $internalRedirect = \URL::to(
            '_paymentgateway/'.OperatorUrlizer::urlize($this).'?pgUuid='.base64_encode($this->uuid)
        );

        return new PaymentResponse($this, $internalRedirect);
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data)
    {
        if ($this->isPaidInStripe && $this->can(Operator::TRANSITION_ACCEPT)) {
            try {
                $this->accept();
            } catch (\Exception $ex) {
                \Log::error($ex->getMessage());
            }

            return \Redirect::to($this->returnUrl);
        }

        return \Redirect::to($this->returnUrl);

    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        $this->setApiKey();

        try {

            $charge = Charge::retrieve($this->chargeId);
            $charge->refunds->create();

        } catch (\Exception $e) {

            return new PaymentResponse($this, null, [$e->getMessage()]);
        }

        // if no exception occured, we assume refund was created

        return new PaymentResponse($this, null);
    }

    /**
     * @param array $data
     *
     * @return string
     * @throws \RuntimeException
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['pgUuid'])) {
            return base64_decode($data['pgUuid']);
        }
        throw new \RuntimeException('Invalid redirect, payment uuid is missing.');

    }

    protected function setApiKey()
    {
        $this->getSettings();
        $testMode = $this->getSettings()->get('stripe.testMode');
        $stripeApiKey = $this->getSettings()->get('stripe.sapiKey');
        if ($testMode) {
            $stripeApiKey = $this->getSettings()->get('stripe.stestApiKey');
        }
        StripeApi::setApiKey($stripeApiKey);
    }

    protected function makeCardErrorMessage(\Stripe\Error\Card $exception)
    {
        $body = $exception->getJsonBody();
        $error = $body['error'];

        $errArr = [];

        $errArr['status'] = $exception->getHttpStatus();
        array_key_exists('type', $error) ? $errArr['type'] = $error['type'] : false;
        array_key_exists('code', $error) ? $errArr['code'] = $error['code'] : false;
        array_key_exists('param', $error) ? $errArr['parameter'] = $error['param'] : false;
        array_key_exists('message', $error) ? $errArr['message'] = $error['message'] : false;

        $message = 'Error from Stripe: ';

        foreach ($errArr as $errKey => $errValue) {
            $message .= $errKey.': '.$errValue.', ';
        }

        return $message;
    }
}
