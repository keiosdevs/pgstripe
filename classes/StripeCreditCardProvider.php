<?php

namespace Keios\PGStripe\Classes;

use Cms\Classes\ComponentBase;
use Exception;
use Flash;
use Keios\PaymentGateway\Components\Orders;
use Keios\PaymentGateway\Contracts\CreditCardProvider;
use Keios\PaymentGateway\Models\Settings;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Log;
use October\Rain\Exception\ValidationException;
use Redirect;

class StripeCreditCardProvider implements CreditCardProvider
{

    public function getVars(Orders $component): array
    {
        $vars = [];
        $settings = Settings::instance();
        $vars['stripe_secure'] = $settings->get('stripe.3dsecure');
        if ($settings->get('stripe.testMode')) {
            $vars['stripe_publickey'] = $settings->get('stripe.testApiKey');
        } else {
            $vars['stripe_publickey'] = $settings->get('stripe.apiKey');
        }

        return $vars;
    }

    public function processSecureCardOrder(Orders $component, $canActOnOrder, $order, $getData)
    {
        if ($canActOnOrder) {
            /**
             * @var PaymentResponse
             */
            try {
                $response = $component->processOrder($order, $getData);
            } catch (ValidationException $e) {
                throw $e;
            } catch (Exception $ex) {
                Log::error('Error while sending payment request: '.$ex->getMessage().$ex->getTraceAsString());

                Flash::error(trans('keios.paymentgateway::lang.text.frontend_send_payment_error'));

                return Redirect::to($component->getRequestRedirectUrl());
            }

            if ($response->isRedirect()) {
                return $response->getRedirect();
            }

            if ($response->isSuccessful()) {
                Flash::success(trans('keios.paymentgateway::lang.text.frontend_order_registered'));
            } elseif ($response->getErrors()) {
                Flash::error(trans('keios.paymentgateway::lang.text.frontend_send_payment_error'));
            } else {
                Flash::success(trans('keios.paymentgateway::lang.text.frontend_order_registered'));
            }

            return Redirect::to($component->getRequestRedirectUrl());
        }

        return Redirect::to($component->property('noAccessRedirect'));
    }
}
