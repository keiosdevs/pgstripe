<?php namespace Keios\PGStripe\Classes;

use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\Core\OrderComponentResolver;
use Keios\PaymentGateway\ValueObjects\Cart;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\PaymentGateway\ValueObjects\CreditCard;

/**
 * Class StripeChargeMaker
 * @package Keios\PGStripe\Classes
 */
class StripeChargeMaker
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var Details
     */
    protected $details;

    /**
     * @var CreditCard
     */
    protected $card;

    /**
     * @var string
     */
    protected $uuid;

    /**
     * StripeChargeMaker constructor.
     *
     * @param Orderable|Cart $cart
     * @param Details        $details
     * @param CreditCard     $card
     * @param string         $uuid
     */
    public function __construct(Orderable $cart, Details $details, CreditCard $card, $uuid)
    {

        $this->cart = $cart;
        $this->details = $details;
        $this->card = $card;
        $this->uuid = $uuid;
    }

    /**
     * @param null $redirect
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    public function make($makeSecure = false, $redirect = null)
    {
        $cost = $this->cart->getTotalGrossCost(true);
        $parsedCard = $this->parseCard($this->card);

        $metaData = [
            'email' => $this->details->getEmail(),
        ];

        $charge = [
            'amount'               => $cost->getAmount(),
            'currency'             => strtolower($cost->getCurrency()->getIsoCode()),
            'description'          => $this->details->getDescription(),
            'source'               => $parsedCard,
            'metadata'             => $metaData,
            'statement_descriptor' => $this->details->get(
                'statement_descriptor',
                substr(trim(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', \Request::getHost())), 0, 22)
            ),
        ];
        if($makeSecure){
            unset($charge['statement_descriptor'], $charge['metadata'], $charge['description'], $charge['source']);
        }
        if ($redirect) {
            //$charge['source'] = $this->card->getSource();
            //$charge['redirect']['return_url'] = $redirect;
            $charge['return_url'] = $redirect;
        }

        if ($this->cart->hasShipping()) {
            $shipping = $this->cart->getShipping();
            $charge['shipping'] = [
                'name'     => $shipping->getName(),
                'cost'     => $shipping->getGrossCost()->getAmountBasic(),
                'currency' => $shipping->getGrossCost()->getCurrency()->getIsoCode(),
                'tax'      => $shipping->getTax().'%',
            ];
        }

        return $charge;
    }

    /**
     * @param CreditCard $card
     *
     * @return array
     */
    protected function parseCard(CreditCard $card)
    {
        return [
            'object'    => 'card',
            'number'    => $card->getNumber(),
            'exp_month' => (int)$card->getExpiryMonth(),
            'exp_year'  => $card->getExpiryYear(),
            'cvc'       => $card->getCvv(),
            'name'      => $card->getFirstName().' '.$card->getLastName(),
        ];
    }
}